/*
Program Name    :   Count Occurrences of a number in a given sorted array.
Author          :   Md. Sakib Muhtadee
Date            :   05/01/2022
*/
#include<bits/stdc++.h>
using namespace std;

int bsearch(int *arr,int n,int x,bool searchFirst)
{
    int l=0,h=n-1;
    int result=-1;
    while(l<=h)
    {
        int mid =(l+h)/2;
        if(arr[mid]==x)
        {
            result=mid;
            if(searchFirst)
            {
                h=mid-1;
            }
            else
            {
                l=mid+1;
            }

        }
        else if(x<arr[mid])
        {
            h=mid-1;
        }
        else
        {
            l=mid+1;
        }
    }
    return result;

}
int main()
{
    int arr[]={10,10,20,30,30,30,33,33,33,33,40,40,44,60,66};
    int n,first,last,cnt=0;
    n=sizeof(arr)/sizeof(arr[0]);

    first=bsearch(arr,n,45,true);

    if(first==-1)
    {
        cout<<cnt<<endl;
    }

    else
    {
        last=bsearch(arr,n,45,false);
        cnt=last-first+1;
        cout<<cnt<<endl;
    }

    return 0;
}
