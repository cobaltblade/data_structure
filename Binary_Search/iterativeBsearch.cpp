/*
Program Name    :   Binary Search (Iterative Method)
Author          :   Md. Sakib Muhtadee
Date            :   04/01/2022
*/

#include<bits/stdc++.h>
using namespace std;

int bsearch(int *arr,int n,int x)
{
    int l,h;
    l=0;
    h=n-1;

    while(l<=h)
    {
        int mid=(l+h)/2;
        if(x==arr[mid])
        {
            return mid;
        }
        else if(x<arr[mid])
        {
            h=mid-1;
        }
        else
        {
            l=mid+1;
        }
    }
    return -1;
}
int main()
{
    int arr[]={10,20,30,33,40,44,66};
    int k,n;
    n=sizeof(arr)/sizeof(arr[0]);
    k=bsearch(arr,n,44);
    cout<<k<<endl;
    return 0;
}

