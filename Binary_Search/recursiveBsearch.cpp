/*
Program Name    :   Binary Search (Recursive Method)
Author          :   Md. Sakib Muhtadee
Date            :   05/01/2022
*/
#include<bits/stdc++.h>
using namespace std;

int bsearch(int *arr,int l,int h,int x)
{
    if(l>h)
    {
        return -1;
    }
    int mid=(l+h)/2;
    if(x==arr[mid])
    {
        return mid;
    }
    else if(x<arr[mid])
    {
        bsearch(arr,l,mid-1,x);
    }
    else
    {
        bsearch(arr,mid+1,h,x);
    }
}

int main()
{
    int arr[]={10,20,30,33,40,44,66};
    int k,n;
    n=sizeof(arr)/sizeof(arr[0]);
    k=bsearch(arr,0,n,45);
    cout<<k<<endl;
    return 0;
}
