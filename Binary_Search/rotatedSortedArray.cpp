/*
Program Name    :   How many times a sorted array rotated?
Author          :   Md. Sakib Muhtadee
Date            :   06/01/2022
*/
#include<bits/stdc++.h>
using namespace std;

int FindRotation(int *arr,int n)
{
    int l=0,h=n-1;
    while(l<=h)
    {
        if(arr[l]<=arr[h])
        {
            return l;
        }

        int mid=(h+l)/2;
        int next=(mid+1)%n;
        int prev=(mid+n-1)%n;//here mid+n will prevent the value from becoming negative value

        if (arr[mid]<=arr[next]&& arr[mid]<=arr[prev])
        {
            return mid;
        }
        else if(arr[mid]<arr[h])
        {
            h=mid-1;
        }
        else
        {
                l=mid+1;
        }

    }
    return -1;
}

int main()
{
    int arr[]={11,12,15,18,20,2,5,6,8};
    int n=sizeof(arr)/sizeof(arr[0]);
    int rotation=FindRotation(arr,n);
    cout<<rotation<<endl;
}
