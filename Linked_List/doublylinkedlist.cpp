#include<bits/stdc++.h>
using namespace std;

class node
 {
 public:
    int data;
    node *next=NULL;
    node *prev=NULL;
 };
class linked_list
{
    node *head=NULL;
public:
    void insert_begining(int data)
    {
        node *temp=new node();
        temp->data=data;
        temp->prev=NULL;
        temp->next=NULL;

        if (head==NULL)
        {
            head=temp;
        }
        else
        {
            head->prev=temp;
            temp->next=head;
            head=temp;
        }

    }
    void insert_at_end(int data)
    {
        node *temp=new node();
        temp->data=data;
        temp->next=NULL;
        temp->prev=NULL;
        if(head==NULL)
        {
            head=temp;
        }
        else
        {
            node *cur=head;
            while (cur->next!=NULL)
            {
                cur=cur->next;
            }
            cur->next=temp;
            temp->prev=cur;
        }

    }
    void insert_at_nth_position(int data,int pos)
    {

            node *temp=new node();
            temp->data=data;
            temp->next=NULL;
            temp->prev=NULL;

            node *cur=head;
            if (pos==0)
            {
                insert_begining(data);
                return;
            }

            while((pos-1)>0)
            {
                if(cur==NULL && pos)
                {
                    cout<<"Not Possible!"<<endl;
                    break;
                }
                cur=cur->next;
                pos--;
            }
            temp->prev=cur;
            temp->next=cur->next;
            cur->next=temp;
    }
    void print()
    {
        if (head==NULL)
        {
            cout<<"Empty List"<<endl;
        }
        else
        {
            node *cur=head;
            while(cur!=NULL)
            {
                cout<<cur->data<<" ";
                cur=cur->next;
            }
            cout<<endl;
        }
    }

};
int main()
{
    linked_list l;
    l.insert_at_end(45);
    l.insert_begining(30);
    l.insert_begining(35);
    l.insert_at_nth_position(34,4);

    l.print();

}
