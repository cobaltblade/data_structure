#include<bits/stdc++.h>
using namespace std;

class Node
{
public:
    int data;
    Node *next=NULL;
};

class List
{
    Node *head=NULL;
public:
    void insert_end(int data)
    {
        Node *temp=new Node();
        temp->data=data;
        if(head==NULL)
        {
            head=temp;
        }
        else
        {
            Node *cur=head;
            while (cur->next!=NULL)
            {
                cur=cur->next;
            }
            cur->next=temp;
        }
    }

    void reverse_list(Node *cur)
    {
        if(cur==NULL)
        {
            return;
        }

        reverse_list(cur->next);
        cout<<cur->data<<" ";
    }
    void rev()
    {
        reverse_list(head);
    }
    void print()
    {
        if (head==NULL)
        {
            cout<<"Empty List"<<endl;
        }
        else
        {
            Node *cur=head;
            while(cur!=NULL)
            {
                cout<<cur->data<<" ";
                cur=cur->next;
            }
            cout<<endl;
        }
    }
};

int main()
{
    List l;
    l.insert_end(45);
    l.insert_end(30);
    l.insert_end(34);
    l.insert_end(32);
    l.insert_end(31);
    l.print();
    l.rev();

}

