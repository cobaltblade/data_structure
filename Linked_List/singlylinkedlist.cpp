#include<bits/stdc++.h>
using namespace std;

class Node
{
public:
    int data;
    Node *next =NULL;
};

class linked_list
{
    Node *head=NULL;
public:
    void insert_begining(int data)
    {
        Node *temp=new Node();
        temp->data=data;
        temp->next=head;
        head=temp;
    }
    void insert_end(int data)
    {
        Node *temp=new Node();
        temp->data=data;
        if(head==NULL)
        {
            head=temp;
        }
        else
        {
            Node *cur=head;
            while (cur->next!=NULL)
            {
                cur=cur->next;
            }
            cur->next=temp;
        }
    }
    void insert_nth_position(int data, int pos)
    {

        if(pos==0)
        {
                insert_begining(data);
        }
        else
        {
            Node *temp=new Node();
            temp->data=data;
            Node *cur=head;

                while(pos>1)
                {
                    if(cur->next==NULL)
                    {
                        cout<<"Out of Bounds"<<endl;
                        return;
                    }
                    cur=cur->next;
                    pos--;

                }
                temp->next=cur->next;
                cur->next=temp;
        }

    }
    void delete_at_the_begining()
    {
        if(head==NULL)
            {cout<<"Empty List"<<endl;}
        else
        {
            cout<<"First element is deleted."<<endl;
            Node *temp=head;
            head=head->next;
            delete(temp);
        }
    }
    void delete_at_the_end()
    {
        if(head==NULL)
            {cout<<"Empty List"<<endl;}
        else
        {
            cout<<"Last element is deleted."<<endl;
            Node *cur=head;
            while(cur->next->next!=NULL)
            {
                cur=cur->next;
            }
            Node*temp=cur->next;
            cur->next=NULL;
            delete(temp);
        }
    }
    void delete_at_nth_position(int pos)
    {
        if(pos==0)
        {
                delete_at_the_begining();
        }
        else
        {
            int k=pos;

            Node *cur=head;
            while(pos>1)
            {
                if(cur->next==NULL)
                {
                    cout<<"Out of Bounds"<<endl;
                    return;
                }
                cur=cur->next;
                pos--;
            }
                Node *temp=cur->next;
                cur->next=cur->next->next;
                delete(temp);
                cout<<"Element No:"<<k<<" is deleted."<<endl;
        }

    }

    void print()
    {
        if (head==NULL)
        {
            cout<<"Empty List"<<endl;
        }
        else
        {
            Node *cur=head;
            while(cur!=NULL)
            {
                cout<<cur->data<<" ";
                cur=cur->next;
            }
            cout<<endl;
        }
    }
};

int main()
{
    linked_list l;
    l.insert_end(45);
    l.insert_begining(30);
    l.insert_nth_position(34,1);
    l.insert_end(32);
    l.insert_begining(31);
    l.print();
    l.delete_at_nth_position(3);
    l.delete_at_the_begining();
    l.delete_at_the_end();
    l.print();
}
