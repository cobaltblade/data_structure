/*
Program Name    :   Calculate x^n MOD m using recursion
Author          :   Md. Sakib Muhtadee
Date            :   08/01/2022
*/


#include<bits/stdc++.h>
using namespace std;

int power(int x,int n)
{
    if(n==0){
        return 1;
    }
    return (x*power(x,n-1));
}

int mod(int x,int n,int m)
{
    int temp=power(x,n/2);
    return ((temp%m)*(temp%m))%m;
}

int main()
{
    int x,n,m;
    cin>>x>>n>>m;
    if(n%2!=0)
    {
        cout<<((x%m)*mod(x,n-1,m))%m<<endl;
    }
    else
    {
        cout<<mod(x,n,m)<<endl;
    }
    return 0;
}
