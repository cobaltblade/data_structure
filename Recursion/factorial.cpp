/*
Program Name    :   Factorial of n
Author          :   Md. Sakib Muhtadee
Date            :   10/01/2022
*/

#include<bits/stdc++.h>
using namespace std;
int memory[15]={0};

int fact(int n)
{
    if(n<=1)
    {
        return 1;
    }
    if (memory[n]!=0)
    {
        return memory[n];
    }
    else
    {
        memory[n]=n*fact(n-1);
        return memory[n];
    }
}

int main()
{
    int n;
    cin>>n;
    cout<<fact(n)<<endl;
    return 0;
}
