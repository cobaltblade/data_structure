/*
Program Name    :   Fibonacci Series(With/Without Memoization)
Author          :   Md. Sakib Muhtadee
Date            :   06/01/2022
*/

#include<bits/stdc++.h>
#include<ctime>
using namespace std;

int f[51];

int fibonacci(int n)
{
    if(n<=1)
    {
        return n;
    }
    return fibonacci(n-1)+fibonacci(n-2);
}

int fibonacciMemoization(int n)
{
    if(n<=1)
    {
        return n;
    }
    if(f[n]!=-1)
    {
      return f[n];
    }
    f[n]=fibonacciMemoization(n-1)+fibonacciMemoization(n-2);
    return f[n];

}
int main()
{
    for(int i=0;i<51;i++)
    {
        f[i]=-1;
    }
    int n;
    cin>>n;
    cout<<fibonacciMemoization(n)<<endl;
    cout<<fibonacci(n)<<endl;

    return 0;
}
