/*
Program Name    :   Bubble Sort Algorithm
Author          :   Md. Sakib Muhtadee
Date            :   04/01/2022
*/

#include<bits/stdc++.h>
using namespace std;
void BubbleSort(int *arr,int n)
{
    int i,j;
    for(i=1;i<n;i++)
    {
        for(j=0;j<n-1;j++)
        {
            if(arr[i]<arr[j])
                swap(arr[i],arr[j]);
        }
    }
}
void print(int *arr,int n)
{
    for(int i=0;i<n;i++)
    {
        cout<<arr[i]<<" ";
    }
    cout<<endl;
}
int main()
{
    int arr[]={2,7,5,1,4,6,0};
    int n=sizeof(arr)/sizeof(arr[0]);
    BubbleSort(arr,n);
    print(arr,n);
    return 0;
}

