/*
Program Name    :
Author          :   Md. Sakib Muhtadee
Date            :
*/

#include<bits/stdc++.h>
using namespace std;


void printArray(int arr[], int size)
{
    int i;
    for (i=0; i < size; i++)
        cout << arr[i] << " ";
    cout << endl;
}
void insertionSort(int arr[],int n)
{
    for(int i=1;i<=(n-1);i++)
    {
        int cur=arr[i];
        int key=i;
        while(key>0&&arr[key-1]>cur)
        {
            arr[key]=arr[key-1];
            key=key-1;
        }
        arr[key]=cur;
    }
}
int main()
{
    int arr[] = {64, 25, 12, 22, 11,70};
    int n = sizeof(arr)/sizeof(arr[0]);
    insertionSort(arr, n);
    cout << "Sorted array: \n";
    printArray(arr, n);
    return 0;
}

