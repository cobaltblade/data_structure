/*
Program Name    :   Merge Sort(Divide and Conquer)
Author          :   Md. Sakib Muhtadee
Date            :   03/01/2022
*/

#include<bits/stdc++.h>
using namespace std;
void Merge(int *arr,int l,int m,int r)
{

    int i,j,k,nl,nr;
    nl=m-l+1;
    nr=r-m;
    int larr[nl];
    int rarr[nr];
    for(i=0;i<nl;i++)
    {
        larr[i]=arr[l+i];
    }
    for(j=0;j<nr;j++)
    {
        rarr[j]=arr[m+1+j];
    }

    i=0;j=0;k=l;

    while(i<nl && j<nr)
    {
        if(larr[i]<=rarr[j])
        {
            arr[k]=larr[i];
            i++;
        }
        else
        {
            arr[k]=rarr[j];
            j++;
        }
        k++;
    }
    while(i<nl)
    {
        arr[k]=larr[i];
        i++;
        k++;
    }
    while(j<nr)
    {
        arr[k]=rarr[j];
        j++;
        k++;
    }
}
void MergeSort(int *arr,int l,int r)
{
    int m;
    if(l<r)
    {
        m=l+(r-l)/2;
        MergeSort(arr,l,m);
        MergeSort(arr,m+1,r);
        Merge(arr,l,m,r);
    }
}
void print(int *arr,int n)
{
    for(int i=0;i<n;i++)
    {
        cout<<arr[i]<<" ";
    }
    cout<<endl;
}

int main()
{
    int arr[]={2,5,1,4};
    int n=sizeof(arr)/sizeof(arr[0]);
    MergeSort(arr,0,n);
    print(arr,n);
    return 0;
}

