/*
Program Name    :   Quick Sort
Author          :   Md. Sakib Muhtadee
Date            :
*/

#include<bits/stdc++.h>
using namespace std;

int Partition(int *arr,int st, int en)
{
    int pivot=arr[en];
    int pIndex=st;
    int i;
    for(i=st;i<en;i++)
    {
        if(arr[i]<pivot)
        {
            swap(arr[i],arr[pIndex]);
            pIndex++;
        }
    }
    swap(arr[pIndex],arr[en]);
    return pIndex;
}
void QuickSort(int *arr,int st,int en)
{
    if(st>=en){return;}
    int pIndex=Partition(arr,st,en);
    QuickSort(arr,st,pIndex-1);
    QuickSort(arr,pIndex+1,en);
}

void print(int *arr,int n)
{
    for(int i=0;i<n;i++)
    {
        cout<<arr[i]<<" ";
    }
    cout<<endl;
}

int main()
{
    int arr[]={2,7,5,1,4,6,0};
    int n=sizeof(arr)/sizeof(arr[0]);
    QuickSort(arr,0,n);
    print(arr,n);
    return 0;
}
