/*
Program Name    :  Selection Sort
Author          :  Md. Sakib Muhtadee
Date            :  02/01/2022
*/

#include<bits/stdc++.h>
using namespace std;

void SelectionSort(int *arr,int n)
{

    int i,j,imin;
    for(i=0;i<(n-1);i++)
    {
        imin=i;

        for(j=i;j<n;j++)
        {
            if (arr[j]<arr[imin])
            {
                imin=j;
            }
        }
        swap(arr[imin],arr[i]);
    }

}
void print(int *arr,int n)
{
    for(int i=0;i<n;i++)
    {
        cout<<arr[i]<<" ";
    }
    cout<<endl;
}

int main()
{
    int arr[]={2,5,1,4};
    int n=sizeof(arr)/sizeof(arr[0]);
    SelectionSort(arr,n);
    print(arr,n);
    return 0;
}
